<?php

/**
* @author: Ткач Сергей
*/
class FilterGd 
{
	/** Apply and deliver the image and clean up */
	function gd_filter_image($image, $filter_name)
	{
		$filter = 'gd_filter_' . $filter_name;
		if (method_exists($this,$filter)) {
			
			
			$im = $this->$filter($image);
			
			return $im;
		}
	}

	/** Apply 'Dreamy' preset */
	function gd_filter_dreamy($im)
	{
		imagefilter($im, IMG_FILTER_BRIGHTNESS, 20);
		imagefilter($im, IMG_FILTER_CONTRAST, -35);
		imagefilter($im, IMG_FILTER_COLORIZE, 60, -10, 35);
		imagefilter($im, IMG_FILTER_SMOOTH, 7);
		$im = $this->gd_apply_overlay($im, 'scratch', 10);
		$im = $this->gd_apply_overlay($im, 'vignette', 100);
		return $im;
	}

	/** Apply 'Blue Velvet' preset */
	function gd_filter_velvet($im)
	{
		imagefilter($im, IMG_FILTER_BRIGHTNESS, 5);
		imagefilter($im, IMG_FILTER_CONTRAST, -25);
		imagefilter($im, IMG_FILTER_COLORIZE, -10, 45, 65);
		$im = $this->gd_apply_overlay($im, 'noise', 45);
		$im = $this->gd_apply_overlay($im, 'vignette', 100);
		return $im;
	}

	/** Apply 'Chrome' preset */
	function gd_filter_chrome($im)
	{
		imagefilter($im, IMG_FILTER_BRIGHTNESS, 15);
		imagefilter($im, IMG_FILTER_CONTRAST, -15);
		imagefilter($im, IMG_FILTER_COLORIZE, -5, -10, -15);
		$im = $this->gd_apply_overlay($im, 'noise', 45);
		$im = $this->gd_apply_overlay($im, 'vignette', 100);
		return $im;
	}

	/** Apply 'Lift' preset */
	function gd_filter_lift($im)
	{
		imagefilter($im, IMG_FILTER_BRIGHTNESS, 50);
		imagefilter($im, IMG_FILTER_CONTRAST, -25);
		imagefilter($im, IMG_FILTER_COLORIZE, 75, 0, 25);
		$im = $this->gd_apply_overlay($im, 'emulsion', 100);
		return $im;
	}

	/** Apply 'Canvas' preset */
	function gd_filter_canvas($im)
	{
		imagefilter($im, IMG_FILTER_BRIGHTNESS, 25);
		imagefilter($im, IMG_FILTER_CONTRAST, -25);
		imagefilter($im, IMG_FILTER_COLORIZE, 50, 25, -35);
		$im = $this->gd_apply_overlay($im, 'canvas', 100);
		return $im;
	}

	/** Apply 'Vintage 600' preset */
	function gd_filter_vintage($im)
	{
		imagefilter($im, IMG_FILTER_BRIGHTNESS, 15);
		imagefilter($im, IMG_FILTER_CONTRAST, -25);
		imagefilter($im, IMG_FILTER_COLORIZE, -10, -5, -15);
		imagefilter($im, IMG_FILTER_SMOOTH, 7);
		$im = $this->gd_apply_overlay($im, 'scratch', 7);
		return $im;
	}

	/** Apply 'Monopin' preset */
	function gd_filter_monopin($im)
	{
		imagefilter($im, IMG_FILTER_GRAYSCALE);
		imagefilter($im, IMG_FILTER_BRIGHTNESS, -15);
		imagefilter($im, IMG_FILTER_CONTRAST, -15);
		$im = $this->gd_apply_overlay($im, 'vignette', 100);
		return $im;
	}

	/** Apply 'Antique' preset */
	function gd_filter_antique($im)
	{
		imagefilter($im, IMG_FILTER_BRIGHTNESS, 0);
		imagefilter($im, IMG_FILTER_CONTRAST, -30);
		imagefilter($im, IMG_FILTER_COLORIZE, 75, 50, 25);
		return $im;
	}

	/** Apply 'Black & White' preset */
	function gd_filter_blackwhite($im)
	{
		imagefilter($im, IMG_FILTER_GRAYSCALE);
		imagefilter($im, IMG_FILTER_BRIGHTNESS, 10);
		imagefilter($im, IMG_FILTER_CONTRAST, -20);
		return $im;
	}

	/** Apply 'Colour Boost' preset */
	function gd_filter_boost($im)
	{
		imagefilter($im, IMG_FILTER_CONTRAST, -35);
		imagefilter($im, IMG_FILTER_COLORIZE, 25, 25, 25);
		return $im;
	}

	/** Apply 'Sepia' preset */
	function gd_filter_sepia($im)
	{
		imagefilter($im, IMG_FILTER_GRAYSCALE);
		imagefilter($im, IMG_FILTER_BRIGHTNESS, -10);
		imagefilter($im, IMG_FILTER_CONTRAST, -20);
		imagefilter($im, IMG_FILTER_COLORIZE, 60, 30, -15);
		return $im;
	}

	/** Apply 'Partial blur' preset */
	function gd_filter_blur($im)
	{
		imagefilter($im, IMG_FILTER_SELECTIVE_BLUR);
		imagefilter($im, IMG_FILTER_GAUSSIAN_BLUR);
		imagefilter($im, IMG_FILTER_CONTRAST, -15);
		imagefilter($im, IMG_FILTER_SMOOTH, -2);
		return $im;
	}

	/** Apply a PNG overlay */
	function gd_apply_overlay($im, $type, $amount)
	{
		$width = imagesx($im);
		$height = imagesy($im);
		$filter = imagecreatetruecolor($width, $height);
		
		imagealphablending($filter, false);
		imagesavealpha($filter, true);
		
		$transparent = imagecolorallocatealpha($filter, 255, 255, 255, 127);
		imagefilledrectangle($filter, 0, 0, $width, $height, $transparent);
		
		$overlay = __DIR__.'/filters/' . $type . '.png';

		list($filterWidth, $filterHeight) = getimagesize($overlay);
		$thumb = imagecreatetruecolor($width, $height);
		imagealphablending($thumb, false);
 		imagesavealpha($thumb,true);
		$png = imagecreatefrompng($overlay);
		imagecopyresized($thumb, $png, 0, 0, 0, 0, $width, $height, $filterWidth, $filterHeight);

		imagecopyresampled($filter, $thumb, 0, 0, 0, 0, $width, $height, $width, $height);
		
		$comp = imagecreatetruecolor($width, $height);
		imagecopy($comp, $im, 0, 0, 0, 0, $width, $height);
		imagecopy($comp, $filter, 0, 0, 0, 0, $width, $height);
		imagecopymerge($im, $comp, 0, 0, 0, 0, $width, $height, $amount);
		
		imagedestroy($comp);
		return $im;
	}
}


