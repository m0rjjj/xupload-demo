<?php

class FileController extends Controller {
	const UPLOAD_PATH = 'upload';
	private $_imageTypes = array('image/png','image/jpeg');

	public function actionResize($url="http://xupload.sergey/img-content/homyak.jpg",$size="100x100",$type="crop")
	{
		$ch = curl_init();      
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   
		curl_setopt($ch, CURLOPT_URL, $url);     
		$result = curl_exec($ch);   
		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if($http_status=="200"){
			$imageSize = getimagesize($url);
			$pathinfo = pathinfo($url);
			if(in_array($imageSize['mime'], $this->imageTypes)){
				$tmpDir = "upload/tmp";
				$filename = md5_file($url).".".$pathinfo['extension'];
				$filePath = $tmpDir."/".$filename;

				if(!is_dir($tmpDir)){
					mkdir($tmpDir, 0777, true);
				}
				copy($url, $filePath);
				Yii::import('ext.image.*');
				$image = new Image($filePath);
				if($image){
					$sizes = explode("x", $size);
					$width = $sizes[0];
					$height = $sizes[1];
					$image = FileHelper::resizeImage($image,$width,$height,$type)
					//$image->crop(200,200)
					// ->filter("velvet")
					// ->filter("chrome")
					// ->filter("lift")
					// ->filter("canvas")
					// ->filter("vintage")
					// ->filter("monopin")
					// ->filter("antique")
					->filter("blackwhite")
					// ->filter("boost")
					// ->filter("sepia")
					->filter("blur")
					->render();
					// $image->render();
					// $filter = new FilterGd;
					// var_dump($filter->gd_filter_image($url,'monopin'));
					//$image->render();
					//$image->save($tmpDir."/".$size.".".$image->image->extension);
				}

			}else{
				throw new CHttpException(500,"Не верный тип файла");
			}
			// $UPimage = new CUploadedFile( $path['basename'], $name, $type, filesize( $name ), 0 );
			// header('Pragma: public');
   //          header('Expires: 0');
   //          header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
   //          header('Content-Transfer-Encoding: binary');
   //          header("Content-type: image/png");
            //header('Content-Length ' . filesize($result));
            //echo $result;
			
		}else{
			throw new CHttpException(500,"Файл не найден");
		}
		curl_close($ch);
	}

	public function getImageTypes(){
		return $this->_imageTypes;
	}

}