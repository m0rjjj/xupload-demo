<?php

class FileHelper extends CComponent {
    const UPLOAD_PATH = "upload";

	public static function saveFile( $model, $source, $field) {
        $modelClass = get_class( $model );
        if(!is_dir( 'upload/' . $modelClass)){
            mkdir( 'upload/' . $modelClass, 0777, true );
        }else{
            if((substr(sprintf('%o', fileperms('upload/'.$modelClass)), -4))!='0777'){
                chmod('upload/'.$modelClass, 0777);
            }
        }

        $options = array();
        if(method_exists($model, 'options')){
            $options = $model->options(); //Запрос опций прописанных в модели
            $options = (isset($options[$field]))?$options[$field]:array(); // Опции поля
        }
        $images = array();

        //---------------------------------------------------------

        $UPimage = $source;
        if ($UPimage !== NULL){
            $path = pathinfo( $UPimage->name );
            if ( $model->id ){
                $id = $model->id;
            }else{
                $id = 'temp'.md5(time());
            }
            //fileHash - не обнулять, он используется как ключ на выходе из функции
            $fileHash = md5(file_get_contents($UPimage->getTempName()));
            $id .= '-'.$fileHash;
            $fileName = $modelClass . '-' .$field .'-' . $id .  '.' . strtolower( $path["extension"] );
            if (strstr($UPimage->type,'image') && $options && is_array($options)){
                Yii::import('ext.image.*');
                $originalFilePath = self::UPLOAD_PATH.'/'.$modelClass.'/original';
                if(!is_dir($originalFilePath)){
                    mkdir($originalFilePath, 0777, true );
                }
                $UPimage->saveAs( $originalFilePath."/".$fileName );
                $imageOrig = new Image( $originalFilePath."/".$fileName );
                foreach ($options as $sizeKeyname => $size){                    
                    $image = $imageOrig;
                    if (!is_dir(self::UPLOAD_PATH.'/'.$modelClass.'/'.$sizeKeyname)){
                        mkdir( self::UPLOAD_PATH.'/' . $modelClass.'/'.$sizeKeyname, 0777, true );
                    }else{
                        if((substr(sprintf('%o', fileperms(self::UPLOAD_PATH.'/'.$modelClass.'/'.$sizeKeyname)), -4))!='0777'){
                           chmod(self::UPLOAD_PATH.'/'.$modelClass.'/'.$sizeKeyname, 0777);
                        }
                    }
                    $image = self::resizeImage($image,$size['width'],$size['type'],$size['type']);

                    $image->save( self::UPLOAD_PATH.'/'.$modelClass.'/'.$sizeKeyname . '/'.$fileName );
                    chmod(self::UPLOAD_PATH.'/'.$modelClass.'/'.$sizeKeyname.'/'.$fileName, 0777);
                }
                //unlink( $originalFilePath );
                unset ( $image );
                unset ( $imageOrig );
            }else{
                //Если для файла не было настроек или он не изображение
                $UPimage->saveAs( self::UPLOAD_PATH.'/' . $modelClass . '/' . $fileName );
            }

            return array(
                'filename'=>$fileName,
                'hash'=>$fileHash,
            );
        }else{
            throw new Exception("File Source is Null");

        }
    }

    public static function resizeImage($image, $width, $height, $type="resize"){
        $w = $image->width;
        $h = $image->height;
        $r = ($width/$height); //соотношение сторон необходимого изображения
        /*Проверка, если <, значит оригинальное изображение выше (длиннее) или уже по соотношению,
         * чем нужное, нужно обрезать его по высоте
         */

        if(($w/$h)<$r) $rt = true; else $rt = false;
        if($type === 'crop' ){ //Если выбрана Обрезка изображения
            if ( $w > $width || $h > $height ) { //Если изображение больше по одному из параметров
                if ( $w>$h ) { //Изображение горизонтальное
                    if ( $rt )
                        $image->resize( $width, $height, 4 );
                    else
                        $image->resize( $width, $height, 3 );
                        $image->crop( $width, $height );
                } else {
                    if ( !$rt ){
                        $nw = $h*$r;
                        $image->crop($nw, $h, 0, round(($w-$nw)/2));
                        $image->resize( $width, $height, 3 );
                    }else{
                        $nw = $w; //new width
                        $nh = $h; // new height
                        while($nw>$width&&(($nw/$r)>$h)){
                            $nw=$nw-1;
                        }
                        $image->crop($nw, $nw/$r, 0 );
                        $image->resize( $width, $height, 3 );
                    }
                }
            }
        } elseif ( $w > $width || $h > $height){
            $image->resize( $width, $height );
        }
        return $image;
    }




}