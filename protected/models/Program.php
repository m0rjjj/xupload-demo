<?php
/**
 * Модель для таблицы "data_Program".
 *
 * Поля:
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $lang
 * @property integer $parent_ProgramCategory_id
 * @property integer $age
 * @property integer $time
 * @property integer $created_at
 * @property integer $status
 *
 * Связи:
 * @property ProgramCategory $parentProgramCategory
 */
class Program extends CActiveRecord
{
	/**
	 * @return Program the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string title of model
	 */
	public static function modelTitle()
	{
		return 'Твпрограммы';
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'data_Program';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('title, image, lang, age, time, created_at, status', 'required'),
			array('parent_ProgramCategory_id, age, time, created_at, status', 'numerical', 'integerOnly'=>true),
			array('title, image', 'length', 'max'=>255),
			array('lang', 'length', 'max'=>10),
			array('image', 'file', 'on'=>'upload'),
			array('id, title, image, lang, parent_ProgramCategory_id, age, time, created_at, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			//'parentProgramCategory' => array(self::BELONGS_TO, 'ProgramCategory', 'parent_ProgramCategory_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'image' => 'Изображение',
			'lang' => 'Lang',
			'parent_ProgramCategory_id' => 'Категория',
			'age' => 'Age',
			'time' => 'Time',
			'created_at' => 'Дата создания',
			'status' => 'Видимость',
		);
	}

	/**
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('parent_ProgramCategory_id',$this->parent_ProgramCategory_id,true);
		$criteria->compare('age',$this->age);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('status',$this->status,true);
        $pagination = array('pageSize'=> 30);
        return new CActiveDataProvider($this,array(
            'criteria'   => $criteria,
            'pagination' => $pagination
        ));
	}
    
    public function beforeValidate() {
	if ($this->created_at==0) {
		$this->created_at=time();	
	}
	if (strstr($this->created_at,'-')) {
            $date=explode('-',$this->created_at);
            $minute = $hour = 0;
            if(isset($_POST['_time']['created_at'])){
                $time = explode(':',$_POST['_time']['created_at']);
                $hour = (int)$time[0];
                $minute = (int)$time[1];
            }
            $this->created_at=mktime( $hour, $minute, 0, $date[1], $date[0], $date[2] );
	}
	if (strstr($this->time,'-')) {
        $date=explode('-',$this->time);
        $minute = $hour = 0;
        if(isset($_POST['_time']['time'])){
            $time = explode(':',$_POST['_time']['time']);
            $hour = (int)$time[0];
            $minute = (int)$time[1];
        }
        $this->time=mktime( $hour, $minute, 0, $date[1], $date[0], $date[2] );
	}
        return true;
    }
    
    public function getLangList()
    {
    	return array(
    		"ru", "kz", "en"
    	);
    }

    public function getLangTitle($lang="")
    {
    	if($lang===""){
    		$lang = $this->lang;
    	}
    	return Yii::app()->system->getLangTitle($lang);
    }

    public function getCategoryList(){
    	return array(
    		"музыка",
    		"кино",
    		"искусство",
    	);
    }

    public function defaultScope() {
        return array(
            'order' => 'created_at DESC',
        );
    }

    public function getPreview($type,$key=0){
    	$images = json_decode($this->image,true);
    	if($images){
	    	if(array_key_exists($key, $images)){
	    		$key = 0;
	    	}
	    	$file = "upload/".__CLASS__."/".$type."/".$images[$key];
	    	if(is_file($file)){
	    	}
	    		return CHtml::image("/".$file,$this->title);
    	}
    	return null;
    }
        
    /**
	 * @return array of model options
	 */     
	public function options()
	{
        return array(
            'image' => array(
                'full' => array(
                    'width' => 600,
                    'height' => 600,
                    'type' => 'resize'
                ),

                'list' => array(
                    'width' => 65,
                    'height' => 39,
                    'type' => 'crop'
                ),

                'sm' => array(
                    'width' => 50,
                    'height' => 50,
                    'type' => 'crop'
                ),
            
                'tm' => array(
                    'width' => 160,
                    'height' => 120,
                    'type' => 'crop'
                ),

            )
        );
	}
    
}
